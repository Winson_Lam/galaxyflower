import numpy as np
from scipy.stats import multivariate_normal

def multivariateGaussian(dataset,mu,sigma):
    p = multivariate_normal(mean=mu, cov=sigma)
    return p.pdf(dataset)
sigma = np.load("sigma.npy")
mu = np.load("mu.npy")
ep = np.load("ep.npy")

p = multivariateGaussian([30, 0.9],mu,sigma)
p < ep

# Import Libraries
import os
import glob
import time
import RPi.GPIO as GPIO
from gpiozero import LightSensor

"""
Initialise the GPIO configuration
"""

# LIGHTS AND BUZZER
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False) # Initialize the GPIO Pins

GPIO.setup(24, GPIO.OUT) #Blue Light
GPIO.setup(18, GPIO.OUT) #RED Light
GPIO.setup(22, GPIO.OUT) #buzzer

# TEMPERATURE SENSOR
os.system("modprobe w1-gpio") # Turns on the GPIO module
os.system("modprobe w1-therm") # Turns on the Temperature module
base_dir = "/sys/bus/w1/devices/" # Find the correct Device file that holds the temperature data
device_folder = glob.glob(base_dir + "28*")[0]
device_file = device_folder + "/w1_slave"

# LDR Sensor
ldr = LightSensor(27)

def read_temp_raw():
    	"""A function that reads the sensors data"""
	f = open(device_file, "r") # opens the temperature device file
	lines = f.readlines() # Returns the text
	f.close()
	return lines

# Convert the value of the sensor into a temperature

def read_temp():
	lines = read_temp_raw() # Read the temperature "device file"

	# While the first line does not contain "YES", wait for 0.2s
	# and then read the device file again.
	while lines[0].strip()[-3:] != "YES":
		time.sleep(0.2)
        lines = read_temp_raw()

	# Look for the position of the "=" in the second line of the evice file.

	equals_pos = lines[1].find("t=")

	# If the "=" is found, convert the rest of the line after the 
	# "=" into degrees Celsius, then degrees Fahrenheit

	if equals_pos != -1:
		temp_string = lines[1][equals_pos+2:]
		temp_c = float(temp_string) / 1000.0
		temp_f = temp_c * 9.0/ 5.0 + 32.0
		return temp_c, temp_f

# Print out the temperature until the program is stopped

recordings = []
while True:
	temp_c, tempf = read_temp()
    	light_intensity = ldr.value

	p = multivariateGaussian([temp_c, light_intensity],mu,sigma)
	print(temp_c, light_intensity, p)    
    	capture = [temp_c, light_intensity, p]
    	recordings.append(capture)
	time.sleep(1)
    
	if p < ep:
	        """ Buzz Red Light if anomaly is detected"""
		GPIO.output(18, GPIO.HIGH) # Red Lights on
		GPIO.output(22, GPIO.HIGH) # Buzzer On
		time.sleep(0.5)
		GPIO.output(18, GPIO.LOW)  
		GPIO.output(22, GPIO.LOW)
        	break
	else:
        	""" Continue with the reading"""
		GPIO.output(24, GPIO.HIGH) # Blue Lights On
		time.sleep(0.5)
		GPIO.output(24, GPIO.LOW)

GPIO.cleanup()

full_recording = np.array(recordings)
np.save("anomaly_detection_algo_full_recording", full_recording)


