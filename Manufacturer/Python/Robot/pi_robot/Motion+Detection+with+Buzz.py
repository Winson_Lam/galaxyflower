
# coding: utf-8

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


def red_light_on():
    GPIO.setup(18, GPIO.OUT) #RED Light
    GPIO.output(18, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(18, GPIO.LOW)

def blue_light_on():
    GPIO.setup(24, GPIO.OUT) #BLUE Light
    GPIO.output(24, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(24, GPIO.LOW)
    
def buzzer_sound_on():
    GPIO.setup(22, GPIO.OUT) #Buzzer
    GPIO.output(22, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(22, GPIO.LOW)


pinpir = 17
print ("PIR Module Test CTRL-C to exit")

# Set pin as input
GPIO.setup(pinpir, GPIO.IN)

# Variables to hold the current and last states

currentstate = 0
previousstate = 0

try: 
	print("Waiting for PIR to settle ...")
	# Loop until PIR output is 0
	while GPIO.input(pinpir) == 1:
		currentstate = 0
    
    	print ("Ready")
    	# Loop until users quits with CTRL-C
    	while True:
    	# Read PIR state
    		currentstate = GPIO.input(pinpir)
	    	# If the PIR is triggered
	    	if currentstate ==1 and previousstate ==0:
	            	red_light_on()
	            	buzzer_sound_on()
	            	print ("	Motion detected!")
	            	# Record previous state
	            	previousstate = 1

	        # If the PIR has returned to ready state
	       	elif currentstate ==0 and previousstate ==1:
	            	print ("	Ready")
	            	previousstate = 0

        # Wait for 10 milliseconds
        time.sleep(0.01)

except KeyboardInterrupt:
    	print ("	Quit")

    	# Reset GPIO settings
    	GPIO.cleanup()



